/**
  @file
  @author Stefan Frings
*/

#include "webservicecontroller.h"
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>

WebServiceController::WebServiceController()
{

}

void WebServiceController::setActiveDicts(vector< sptr< Dictionary::Class > > const *activeDicts)
{
    m_pActiveDicts = activeDicts;
}

void WebServiceController::service(HttpRequest& request, HttpResponse& response)
{

    // set some headers
    response.setHeader("Content-Type", "application/json; charset=ISO-8859-1");
    response.setCookie(HttpCookie("wsTest","CreateDummyPerson",600));

    QJsonObject recordObject;

   // QJsonObject recordObject = new QJsonObject();
        recordObject.insert("FirstName", "John");
        recordObject.insert("LastName", QJsonValue::fromVariant("Doe"));
        recordObject.insert("Age", QJsonValue::fromVariant(43));

        QJsonObject addressObject;
        addressObject.insert("Street", "Downing Street 10");
        addressObject.insert("City", "London");
        addressObject.insert("Country", "Great Britain");
        recordObject.insert("Address", addressObject);

        QJsonArray phoneNumbersArray;
        phoneNumbersArray.push_back("+44 1234567");
        phoneNumbersArray.push_back("+44 2345678");
        recordObject.insert("Phone Numbers", phoneNumbersArray);

        QJsonDocument doc(recordObject);

    QString dp(doc.toJson(QJsonDocument::Compact));
    //QString dp = WebServiceController::getDummyPerson();
    QByteArray ba = dp.toLocal8Bit();
    const char *baChar = ba.data();
    response.write(ba);
}

