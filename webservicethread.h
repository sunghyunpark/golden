#ifndef WEBSERVICETHREAD_H
#define WEBSERVICETHREAD_H

#include <QThread>
#include "wordfinder.hh"
#include "httpserver/httplistener.h"
#include "templateengine/templatecache.h"
#include "httpserver/httpsessionstore.h"
#include "httpserver/staticfilecontroller.h"
#include "logging/filelogger.h"
#include "RequestMapper.h"
#include "instances.hh"

using std::vector;

class MainWindow;
class WebServiceThread : public QObject// : public QThread
{
public:
    WebServiceThread(MainWindow const *parent/*, vector< sptr< Dictionary::Class > > const  activeDicts*/);
	void setActiveDicts(vector< sptr< Dictionary::Class > > const activeDicts, Instances::Groups groupInstances);
    void run();
private:
    WordFinder wordFinder;
    /** Cache for template files */
    stefanfrings::TemplateCache* templateCache;

    /** Storage for session cookies */
    stefanfrings::HttpSessionStore* sessionStore;

    /** Controller for static files */
    stefanfrings::StaticFileController* staticFileController;

    /** Redirects log messages to a file */
    stefanfrings::FileLogger* logger;

    const MainWindow *mw;

    QString searchConfigFile();

    vector< sptr< Dictionary::Class > > m_pActiveDicts;
	Instances::Groups m_GroupInstances;
    //void run();
};

#endif // WEBSERVICETHREAD_H
