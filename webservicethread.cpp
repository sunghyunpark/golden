#include "webservicethread.h"

#include <QDir>


WebServiceThread::WebServiceThread(MainWindow const *parent) :
    wordFinder( (QObject*)parent ),
    mw(parent)
{
    //m_pActiveDicts = 0;

   // m_pActiveDicts = activeDicts;
}

void WebServiceThread::setActiveDicts(vector< sptr< Dictionary::Class > > const activeDicts, Instances::Groups groupInstances)
{
    m_pActiveDicts = activeDicts;
	m_GroupInstances = groupInstances;
}

QString WebServiceThread::searchConfigFile()
{
    QString binDir=QCoreApplication::applicationDirPath();
    QString appName=QCoreApplication::applicationName();
    QString fileName(appName+".ini");

    QStringList searchList;
    searchList.append(binDir);
    searchList.append(binDir+"/etc");
    searchList.append(binDir+"/../etc");
    searchList.append(binDir+"/../../etc"); // for development without shadow build
    searchList.append(binDir+"/../"+appName+"/etc"); // for development with shadow build
    searchList.append(binDir+"/../../"+appName+"/etc"); // for development with shadow build
    searchList.append(binDir+"/../../../"+appName+"/etc"); // for development with shadow build
    searchList.append(binDir+"/../../../../"+appName+"/etc"); // for development with shadow build
    searchList.append(binDir+"/../../../../../"+appName+"/etc"); // for development with shadow build
    searchList.append(QDir::rootPath()+"etc/opt");
    searchList.append(QDir::rootPath()+"etc");

    foreach (QString dir, searchList)
    {
        QFile file(dir+"/"+fileName);
        if (file.exists())
        {
            // found
            fileName=QDir(file.fileName()).canonicalPath();
            qDebug("Using config file %s",qPrintable(fileName));
            return fileName;
        }
    }

    // not found
    foreach (QString dir, searchList)
    {
        qWarning("%s/%s not found",qPrintable(dir),qPrintable(fileName));
    }
    //qFatal("Cannot find config file %s",qPrintable(fileName));
    return 0;
}

void WebServiceThread::run()
{
    // Find the configuration file
    QString configFileName=searchConfigFile();

    // Configure logging into a file
    /*
    QSettings* logSettings=new QSettings(configFileName,QSettings::IniFormat,&app);
    logSettings->beginGroup("logging");
    FileLogger* logger=new FileLogger(logSettings,10000,&app);
    logger->installMsgHandler();
    */

    // Configure template loader and cache
    QSettings* templateSettings=new QSettings(configFileName,QSettings::IniFormat, this);
    templateSettings->beginGroup("templates");
    templateCache = new stefanfrings::TemplateCache(templateSettings, this);

    // Configure session store
    QSettings* sessionSettings=new QSettings(configFileName,QSettings::IniFormat, this);
    sessionSettings->beginGroup("sessions");
    sessionStore=new stefanfrings::HttpSessionStore(sessionSettings, this);

    // Configure static file controller
    QSettings* fileSettings=new QSettings(configFileName,QSettings::IniFormat, this);
    fileSettings->beginGroup("docroot");
    staticFileController=new stefanfrings::StaticFileController(fileSettings, this);

    // Configure and start the TCP listener
    QSettings* listenerSettings=new QSettings(configFileName,QSettings::IniFormat, this);
    listenerSettings->beginGroup("listener");
    RequestMapper* rm = new RequestMapper(mw);
    rm->setActiveDicts(m_pActiveDicts);
    new stefanfrings::HttpListener(listenerSettings, rm, this);

    qWarning("WebServiceThread has started");


   // qWarning("Application has stopped");

}
